// 전역 변수( 두 개의 제품에 대한 정보
var first_btn;
var second_btn;
var url = "http://localhost:5000/"

get_product_info()

// 2개의 버튼 초기화 함수
function get_product_info(){
    $.ajax({
        url:url,
        headers:{
            'Content-Type': "application/json",
            'Access-Control-Allow-Origin':'*'
        },
        method: 'GET',
        crossDomain:true,
        success: function(data){
            if(data.state===200){
                first_btn = data.body.first_item
                second_btn = data.body.second_item
                $('#first-btn').text(first_btn.name)
                $('#second-btn').text(second_btn.name)
            }else{
                alert("Error!!!")
            }
        }
    })
}

// 버튼 클릭 했을 때 결과 가져오는 함수
function get_result(){
        $('.btn').click(function(){
            var id_check = $(this).attr('id')
            console.log(id_check)
            var check_num=0
            if(id_check === 'first_btn'){check_num=1}
            else if(id_check === 'second_btn'){check_num=2}
            $.ajax({
                url:url+'?first_id='+first_btn["id"]+'?second_id='+second_btn["id"]+'?selected='+check_num,
                headers:{
                    'Content-Type': "application/json",
                    'Access-Control-Allow-Origin':'*'
                },
                method: 'GET',
                crossDomain:true,
                success: function(data){
                    if(data.state===200){
//                        성공 여부만 text로 받아옴
                        alert(data.body.result)
                    }else{
                        alert("Error!!!")
                    }
                }
            })
        })
}
